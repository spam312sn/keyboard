# Gizmo Engineering GK6

![GK6](GK6.jpg)

Keyboard maintainer: [federicoweber](https://github.com/federicoweber)

Hardware supported: `ATmega32U4`, `Gizmo Engineering GK6`

Hardware availability: [Gizmo Engineering](https://gizmo.engineering/)

# Layout

![Layout](layout.png)

# Copying this layout:

1. [Install QMK](https://docs.qmk.fm/#/getting_started_build_tools)
2. Enter you username in QMK configuration `qmk user.name=<USERNAME>`
3. Create user directory `qmk new-keymap -kb gizmo_engineering/gk6`
4. Copy `src/info.json` to `<QMK REPOSITORY>/keyboards/gizmo_engineering/gk6/` with replace
5. Copy `src/keymap.c` to `<QMK REPOSITORY>/keyboards/gizmo_engineering/gk6/keymaps/<USERNAME>` with replace

# Applying this layout

**IMPORTANT! I am not responsible for any damage to your keyboard that may occur during firmware flash. You may follow further instructions only at your own risk**

1. Download [QMK Toolbox](https://github.com/qmk/qmk_toolbox)
2. Restart keyboard in bootloader mode (push the button between keys in top right corner)
3. Select `ATmega32U4` as microcontroller
4. Select local file `dist/firmware.hex`
5. Flash keyboard

# Notes

RGB is disabled in this firmware

You can upload `src/layout.json` to www.keyboard-layout-editor.com for edit
